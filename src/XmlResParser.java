import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Created by WOXAPP on 14.01.2016.
 */
public class XmlResParser {

    private DocumentBuilderFactory dbf;
    private DocumentBuilder db;

    public XmlResParser() throws ParserConfigurationException {

        dbf = DocumentBuilderFactory.newInstance();
        db = dbf.newDocumentBuilder();
    }

    private Document createDocXml(String patchFile) throws Exception {

        FileInputStream fis = new FileInputStream(patchFile);
        InputSource is = new InputSource(fis);

        Document document =  db.parse(is);
        return document;
    }

    public List<String> getListResImgName(String patchFile) throws Exception {
        Document node = createDocXml(patchFile);
        List<String> result = new ArrayList<>();
        NodeList arrey =  node.getElementsByTagName("item");

        for (int i = 0; i < arrey.getLength(); i++) {
            Node item = arrey.item(i);
            result.add(item.getTextContent());
        }

        return result;
    }
}

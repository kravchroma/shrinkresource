import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    private final static String PATCH_LOGOS = "app\\src\\main\\res\\values\\logos.xml";
    private final static String PATCH_IMG = "app\\src\\main\\res\\drawable-xxhdpi\\";
    private static int countDel = 0;

    private final static String[] SYSTEM_IMG = readFile();
            /*new String[] {
            "msh.png",
            "native_ad_test.png",
            "swipe_down_icon.png",
            "swipe_left_icon.png",
            "swipe_right_icon.png",
            "imgblock_empty_showcase.png",
    };*/

    private static String[] readFile() {
        List<String> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("file_res.txt")))
        {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                list.add(sCurrentLine);
                System.out.println(sCurrentLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        Object[] objects = list.toArray();
        return Arrays.copyOf(objects, objects.length, String[].class);
    }

    public static void main(String[] args) {

        System.out.println("Cleaning is not necessary resources...");
        Observable.just(PATCH_LOGOS)
                .flatMap(new Func1<String, Observable<List<String>>>() {
                    @Override
                    public Observable<List<String>> call(String s) {
                        List<String> list = new ArrayList<>();
                        try {
                            XmlResParser xmlResParser = new XmlResParser();
                            list = xmlResParser.getListResImgName(s);
                        } catch (ParserConfigurationException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        return Observable.just(list);
                    }
                }).flatMap(new Func1<List<String>, Observable<List<String>>>() {
                    @Override
                    public Observable<List<String>> call(List<String> strings) {
                        for(int i = 0; i < strings.size(); i ++)
                            strings.set(i, strings.get(i).replace("@drawable/","") + ".png");
                        return Observable.just(strings);
                    }
                }).flatMap(new Func1<List<String>, Observable<List<String>>>() {
                    @Override
                    public Observable<List<String>> call(List<String> strings) {
                        //список на удаление.
                        String list[] = new File(PATCH_IMG).list();
                        List<String> res = new ArrayList<>();
                        res.addAll(Arrays.asList(list));
                        for(String item : list) {
                            if(strings.contains(item) || Arrays.asList(SYSTEM_IMG).contains(item)) {
                                res.remove(item);
                            }
                        }

                        return Observable.just(res);
                    }
                }).flatMap(new Func1<List<String>, Observable<String>>() {
                    @Override
                    public Observable<String> call(List<String> strings) {
                        for(int i = 0; i < strings.size(); i++) {
                            strings.set(i, PATCH_IMG + strings.get(i));
                        }
                        return Observable.from(strings);
                    }
                })
                .subscribe(new Subscriber<String>() {

                    @Override
                    public void onCompleted() {
                        System.out.println("FINISH! DELETE COUNT: " + countDel);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();
                    }

                    @Override
                    public void onNext(String string) {
                        File file = new File(string);
                        if(file.delete()) {
                            System.out.println("DELETED: " + string);
                            countDel ++;
                        } else {
                            System.out.println("COULD NOT DELETE: " + string);
                        }
                    }
                });
    }
}
